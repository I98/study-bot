import json
from os import getcwd, getenv
from os.path import abspath, join

config_file = abspath(getenv('CONFIG', '/config.json'))
cfg = json.load(open(config_file))

res = ''
for key in cfg['lectures'].keys():
    cmd = key.lower().replace(' ', '_')
    res += f'{cmd} - { cfg["lectures"][key]["full_name"]} \n'


res += f'ex - Overview of all Exercises \n'
res += f'info - show info \n'
res += f'all - show all lectures\n'
res += f'start - start and thus fix this bot to the chat this command is written in \n'

print(res, flush=True)
