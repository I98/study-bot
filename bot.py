import sys
import time
import random
import datetime
import uuid
import os
import telepot
from os import getcwd, getenv
from os.path import abspath, join
import json

from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

DAY_MAP = ['Sunday', 'Monday', 'Tuesday',
           'Wednesday', 'Thursday', 'Friday', 'Saturday']

config_file = abspath(getenv('CONFIG', '/config.json'))
data_file = abspath(getenv('DATA', '/data/data.json'))

with open(config_file) as f:
    cfg = json.load(f)

data = {}
if os.path.exists(data_file):
    with open(data_file, 'r+') as f:
        data = json.load(f)

if not data.get('done'):
    data['done'] = {}

for key, item in cfg['lectures'].items():
    if not data['done'].get(key):
        data['done'][key] = {}
    for ex_key in item.get('exercises', []):
        if not data['done'][key].get(ex_key):
            data['done'][key][ex_key] = []

with open(data_file, 'w+') as f:
    json.dump(data, f)


def strip_at_from_msg(msg):
    splitted = msg.split('@')

    if len(splitted) == 1:
        return msg
    elif len(splitted) == 2:
        rest = splitted[1].split(' ')[1:] or ['']
        assemble = [splitted[0]]
        assemble.extend(rest)
        return ' '.join(assemble)
    else:
        raise Exception


def date_to_string(date):
    if 0 <= date[0] < 7 and 0 <= date[1][0] < 24 and 0 <= date[1][1] < 24:
        return f'{DAY_MAP[date[0]]}, {date[1][0]}\.00 \- {date[1][1]}\.00'
    else:
        raise Exception


def lec_info(lec):
    text = ''
    for info_key, info_value in lec.get('info', []).items():
        if type(info_value) == dict and info_value.get('link', False):
            text += f'{info_key}: [link]({info_value["link"]}) \n'
        else:
            text += f'{info_key}: {info_value} \n'

    lessons = lec.get('lessons', [])
    if len(lessons) > 0:
        text += f'_Lessons_: \n'
        for value in lessons:
            text += f'     \* {date_to_string(value)}\n'

    exercises_sessions = lec.get('exercises_sessions', [])
    if len(exercises_sessions) > 0:
        text += f'_Exercise Sessions_: \n'
        for value in exercises_sessions:
            text += f'     \* {date_to_string(value)}\n'

    return text


def ex_info(key, lec):
    with open(data_file) as f:
        data = json.load(f)

    text = ''
    ctr = 1
    for ex_key, count in lec.get('exercises', {}).items():
        text += f'_{ctr} {ex_key}_: \n'
        ctr += 1
        for i in range(1, count + 1):
            status = '*open*'
            if i in data['done'][key][ex_key]:
                status = '_done_'
            text += f'     {i}: \[{status}\]\n'
    return text


def ex_undo(key, lec, cat, no):
    with open(data_file) as f:
        data = json.load(f)

    cat_name = ''
    counter = 0
    cat -= 1

    if not (0 <= cat < len(data['done'][key].keys())):
        return 'wrong category'

    for cat_name_done, item in data['done'][key].items():
        if counter == cat and no in item:
            if not (0 <= cat < lec['exercises'][cat_name_done]):
                return 'error in key'
            if no in item:
                item.remove(no)
                cat_name = cat_name_done
                break
        counter += 1

    with open(data_file, 'w') as f:
        json.dump(data, f)

    return f'_Removed {no} from Category {cat_name}_ \n' + ex_info(key, lec)


def ex_do(key, lec, cat, no):
    with open(data_file) as f:
        data = json.load(f)

    cat_name = ''
    ct = 0
    cat -= 1

    if not (0 <= cat < len(data['done'][key].keys())):
        return 'wrong category'

    for k, item in data['done'][key].items():
        if ct == cat:
            if not (0 <= cat < lec['exercises'][k]):
                return 'error in key'
            if no not in item:
                item.append(no)
                cat_name = k
                break
        ct += 1

    with open(data_file, 'w') as f:
        json.dump(data, f)

    return f'_Added {no} from Category {cat_name}_ \n' + ex_info(key, lec)


def ex_summary():
    with open(data_file) as f:
        data = json.load(f)

    text = '*Exercise Summary* \nOpen Exercises: \n'
    for lec_key, lec in cfg['lectures'].items():
        text += f'{lec["full_name"]}:\n'
        ct = 1
        for ex_key, count in lec.get('exercises', {}).items():
            text += f'     _{ct} {ex_key}_: \['
            for i in range(1, count + 1):
                if i not in data['done'].get(key,[]).get(ex_key,[]):
                    text += f'{i}, '
            text = text[:-2]
            text += f'\] of {count}\n'
            ct += 1
        if not lec.get('exercises', False):
            text += '     _No exercises specified_'
    return text


def lect(cmd, key):
    text = ''
    lec = cfg["lectures"][key]

    text += f'*{lec["full_name"]}* \n'
    if len(cmd) == 0:
        text += lec_info(lec)
        return True, text
    elif cmd[0] == 'ex':
        if len(cmd) == 1:
            text += ex_info(key, lec)
            return True, text
        elif (cmd[1] == 'do' and len(cmd) == 4):

            text += ex_do(key, lec, int(cmd[2]), int(cmd[3]))
            return True, text
        elif (cmd[1] == 'undo' and len(cmd) == 4):
            text += ex_undo(key, lec, int(cmd[2]), int(cmd[3]))
            return True, text
        else:
            return False, ''

    else:
        return False, ''

def handle(text, chat_id):
    command = strip_at_from_msg(text.lower())

    cmd_split = command.split(' ')
    if cmd_split[-1] == '':
        cmd_split = cmd_split[:-1]

    for key in cfg['lectures'].keys():
        main_cmd = cmd_split[0][1:]
        cmd_lect = key.lower().replace(' ', '_')
        if main_cmd == cmd_lect:
            if data.get('message_id') == chat_id:
                valid_cmd, ret_msg = lect(cmd_split[1:], key)
                if valid_cmd:
                    return ret_msg
            else:
                return "Your are not authorized to write to the bot from that chat"
        elif main_cmd == 'ex':
            if data.get('message_id') == chat_id:
                return ex_summary()
            else:
                return "Your are not authorized to write to the bot from that chat"
        elif main_cmd == 'info':
            return 'info'
        elif main_cmd == 'all':
            res = ''
            for key in cfg['lectures'].keys():
                _, t = lect([], key)
                res +=  t + '\n'
            return res
        elif main_cmd == 'start':
            if not data.get('message_id'):
                data['message_id'] = chat_id

                with open(data_file, 'w+') as f:
                    json.dump(data, f)

                return 'Successfully registered'

            else:
                return 'This bot is already registered to another user'

    return 'invalid cmd'


def reply(msg):

    print(msg)
    if msg.get('data'):
        text = msg['data']
        chat_id = msg['message']['chat']['id']
        print('yayy ' + text)
    else:
        text = msg['text']
        chat_id = msg['chat']['id']

    keyboard = InlineKeyboardMarkup(inline_keyboard=[
                   [InlineKeyboardButton(text='Press me', callback_data='press')],
               ])

    bot.sendMessage(chat_id, 'Use inline keyboard', reply_markup=keyboard)

    res = handle(text, chat_id)
    bot.sendMessage(chat_id, res, parse_mode='MarkdownV2',
                    disable_web_page_preview=True)


bot = telepot.Bot(os.getenv('TOKEN'))
bot.message_loop(reply)
print('I am listening ...')

while 1:
    time.sleep(10)
