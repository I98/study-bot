FROM python:latest

WORKDIR /app
COPY . /app

VOLUME /data

RUN pip install -r requirements.txt

RUN mkdir out
CMD python bot.py